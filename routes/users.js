const express = require('express');
const router = express.Router();
const argon2 = require('argon2');
const { QueryTypes } = require('sequelize');
const jwt = require('jsonwebtoken');
const passportJWT = require('../middlewares/passport-jwt');

const model = require('../models/index');

//get profile current user login
/* /api/v1/users/profile */
router.get('/profile',[ passportJWT.isLogin],async function(req, res, next) {
  
  const user = await model.User.findByPk(req.user.user_id);
  return res.status(200).json({    
    message:'sucess',
    user: {
      id: user.id,
      fullname: user.fullname,
      email: user.email,
      created_at: user.created_at

    }
  });
});


/* /api/v1/users/ */
router.get('/',async function(req, res, next) {
  //res.send('respond with a resource');
  // const users = await model.User.findAll({
  //   //attributes: ['id','fullname']
  //   attributes: { exclude: ['password']},
  //   order: [['id', 'desc']]
  // }); 
  let sql = 'SELECT id, fullname, email FROM `users` order by id desc';
  const users = await model.sequelize.query(sql, { 
    type: QueryTypes.SELECT
  });

  
  const totalUsers = await model.User.count();

   return res.status(200).json({
     total: totalUsers,
     data: users
   });
});

/* /api/v1/users/register/ */
router.post('/register',async function(req, res, next) {
  //res.send('respond with a resource');
  const { fullname, email, password } = req.body; 
  //1. check email
  const user = await model.User.findOne({ where: {email : email}});
  if (user !== null){
    return res.status(400).json({message: 'มีผู้ใช้งานอีเมล์นี้แล้ว'});
  }

  //2. encrypt password
  const passwordHash = await argon2.hash(password);

  //3. save to table users

  const newUser = await model.User.create({
    fullname: fullname,
    email:email,
    password: passwordHash
  });

  return res.status(201).json({
    user: {
      id: newUser.id,
      fullname: newUser.fullname
    },
     message:'ลงทะเบียนสำเร็จ'
   });
});

/* /api/v1/users/login/ */
router.post('/login',async function(req, res, next) {
  const { email, password } = req.body; 
  //1. check email in table
  const user = await model.User.findOne({ where: {email : email}});
  if (user === null){
    return res.status(404).json({message: 'ยังไม่ได้ register'});
  }

  //2. compare password
  const inValid = await argon2.verify(user.password, password);

  if (!inValid){
    return res.status(401).json({message: 'รหัสผ่านไม่ถูกต้อง'});
  }
  //3. JWT
  const token = jwt.sign({ user_id: user.id }, process.env.JWT_KEY, { expiresIn: '7d' });
    
  return res.status(200).json({    
     message:'เข้าระบบสำเร็จ',
     access_token: token
   });
});

module.exports = router;
